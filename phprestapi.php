<?php
require_once "koneksi.php";

	if(function_exists($_GET['function']))	{
		$_GET['function']();
	}
	
	function get_rs()	{
		global $connect;
		$query = $connect->query("SELECT * FROM rs");
		while($row=mysqli_fetch_object($query))
		{
			$data[] =$row;
		}
		$response=array(
			'status' =>1,
			'message' => 'Success',
			'data' =>$data
			);
			header('Content-Type: application/json');
			echo json_encode($response);
	}
	
	function get_rs_Nama_Rumah_Sakit()
	{
		global $connect;
		if (!empty($_GET["Nama_Rumah_Sakit"])) {
			$Nama_Rumah_Sakit = $_GET["Nama_Rumah_Sakit"];
		}
		$query="SELECT * FROM rs WHERE Nama_Rumah_Sakit = $Nama_Rumah_Sakit;
		$result = $connect->query($query);
		while ($row = mysqli_fetch_object($result))
		{
			$data[] = $row;
		}
		if($data)
		{
			$response = array(
				'status'=>1,
				'message'=>'Berhasil',
				'data'=> $data
				);
		}
		
		header('Content-Type: application/json');
		echo json_encode ($response);
	}
	
	function insert_rs()
	{
		global $connect;
		$check = array('Nama_Rumah_Sakit'=> '', 'Jenis_Rumah_Sakit' => '', 'Alamat_Rumah_Sakit' => '', 'Kelurahan' => '', 'Kecamatan' => '', 'Kota/Kab'=> '', 'Kode_Pos' => '', 'Nomor_Telepon' => '', 'Nomor_Fax'=> '', 'Website' => '', 'Email'=> '');
		$check_match = count(array_intersect-key($_POST, $check));
		if($check_match == count($check)) {
			
			$result = mysqli_query($connect, "INSERT INTO rs SET
			Nama_Rumah_Sakit = '$_POST[Nama_Rumah_Sakit]',
			Jenis_Rumah_Sakit = '$_POST[Jenis_Rumah_Sakit]',
			Alamat_Rumah_Sakit = '$_POST[Alamat_Rumah_Sakit]',
			Kelurahan = '$_POST[Kelurahan]',
			Kecamatan = '$_POST[Kecamatan]',
			Kota/Kab = '$_POST[Kota/Kab]',
			Kode_Pos = '$_POST[Kode_Pos]',
			Nomor_Telepon = '$_POST[Nomor_Telepon]',
			Nomor_Fax = '$_POST[Nomor_Fax]',
			Website = '$_POST[Website]',
			Email = '$_POST[Email]'");
			
			if($result)
			{
				$response=array(
				'status' => 1,
				'message' => "Insert Berhasil'
				);
			}
			else
			{
				$response=array(
				'status' => 0,
				'message' =>'Gagal Insert'
				);
			}
		}else {
			$response=array(
			'status'=> 0,
			'message' => ' Salah Parameter'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}
	
	function update_rs()
	{
		global $connect;
		if (!empty($_GET["Nama_Rumah_Sakit"])) {
			$Nama_Rumah_Sakit = $_GET["Nama_Rumah_Sakit"];
		}
		
		$check = array('Jenis_Rumah_Sakit' => '', 'Alamat_Rumah_Sakit' => '', 'Kelurahan' => '', 'Kecamatan' => '', 'Kota/Kab'=> '', 'Kode_Pos' => '', 'Nomor_Telepon' => '', 'Nomor_Fax'=> '', 'Website' => '', 'Email'=> '');
		$check_match = count(array_intersect-key($_POST, $check));
		if($check_match == count($check)) {
			
			$result = mysqli_query($connect, "UPDATE rs SET
			Jenis_Rumah_Sakit = '$_POST[Jenis_Rumah_Sakit]',
			Alamat_Rumah_Sakit = '$_POST[Alamat_Rumah_Sakit]',
			Kelurahan = '$_POST[Kelurahan]',
			Kecamatan = '$_POST[Kecamatan]',
			Kota/Kab = '$_POST[Kota/Kab]',
			Kode_Pos = '$_POST[Kode_Pos]',
			Nomor_Telepon = '$_POST[Nomor_Telepon]',
			Nomor_Fax = '$_POST[Nomor_Fax]',
			Website = '$_POST[Website]',
			Email = '$_POST[Email]' WHERE Nama_Rumah_Sakit = $Nama_Rumah_Sakit");
			
			if($result)
			{
				$response=array(
				'status' => 1,
				'message' => "Update Berhasil'
				);
			}
			else
			{
				$response=array(
				'status' => 0,
				'message' =>'Gagal Update'
				);
			}
		}else {
			$response=array(
			'status'=> 0,
			'message' => ' Salah Parameter'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}
	
	function delete_rs()
	{
		global $connect;
		$Nama_Rumah_Sakit = $_GET['Nama_Rumah_Sakit'];
		$query = "DELETE FROM rs WHERE Nama_Rumah_Sakit=".$Nama_Rumah_Sakit;
		if(mysqli_query($connect, $query))
		{
			$response= array(
			'status' => 1,
			'message' => "Delete Berhasil'
			);
		}
		else
		{
			$response=array(
			'status' =>0,
			'message' => 'Gagal Delete'
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}
?>	